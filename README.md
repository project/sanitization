CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

A module allow to sanitize the site name, email and user email address based
on environment. It's help the developers to take PROD database backup and
sanittize it then use to next phase development.

Below are section to being sanitize:

* Site Name
* Site Email Address
* AWS S3 Settings
* AWS SMTP Settings

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/sanitization

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/sanitization

REQUIREMENTS
-------------

* This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install the Sanitization module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------

* Customize the sanitization settings in Administration » Configuration »
   People » Sanitization settings.

MAINTAINERS
-----------

Current maintainers:
 * Vernit Gupta - https://www.drupal.org/u/vernit
