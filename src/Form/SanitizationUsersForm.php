<?php

namespace Drupal\sanitization\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sanitization User form.
 */
class SanitizationUsersForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database,
  ConfigFactoryInterface $config_factory,
  EntityTypeManagerInterface $entity_manager) {
    $this->database          = $database;
    $this->configFactory     = $config_factory;
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sanitization_users_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $roles       = [];
    $roleStorage = $this->entityTypeManager->getStorage('user_role');
    $allRoles    = $roleStorage->loadMultiple();
    foreach ($allRoles as $rid => $roledata) {
      $roles[$rid] = $rid;
    }

    $form['sanitize_roles'] = [
      '#type'          => 'checkboxes',
      '#options'       => $roles,
      '#default_value' => array_values($roles),
      '#description'   => $this->t('Deselect the roles for removing sanitization'),
    ];

    $form['sanitize_users'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Sanitize Users'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $connection  = $this->database;
    $operations  = [];
    $defaultUids = [0, 1];
    $roles       = [];

    // Fetch the excluded uids to santization.
    $sanitizationConfig = $this->configFactory->get('sanitization.settings');
    $sanitizeEnv        = $sanitizationConfig->get('sanitization_env');
    $envUidConfig       = $sanitizeEnv . '_excluded_uids';
    $uidList            = $sanitizationConfig->get($envUidConfig);
    $excludeUids        = explode(',', $uidList);

    $uids = array_merge($defaultUids, $excludeUids);

    $query = $connection->select('users', 'users');
    $query->fields('users', ['uid']);
    $query->leftjoin('user__roles', 'roles', 'roles.entity_id = users.uid');
    $query->condition('users.uid', $uids, 'NOT IN');

    if (!empty($roles)) {
      $query->condition('roles.roles_target_id', $roles, 'IN');
    }

    $queryRes = $query->execute();

    if (isset($queryRes)) {
      while ($user = $queryRes->fetchObject()) {
        $operations[] = [
          '\Drupal\sanitization\Form\SanitizationUsersForm::updateEmails',
          [$this, $user->uid],
        ];
      }
    }

    // Prepare Batch data for processing.
    $batch = [
      'title' => $this->t('Processing users email address'),
      'operations' => $operations,
      'finished' => '\Drupal\sanitization\Form\SanitizationUsersForm::updateEmailsFinished',
    ];

    batch_set($batch);
  }

  /**
   * Process batch bulk data.
   */
  public static function updateEmails($batchObj, $uid, &$context) {

    $userStorage = $batchObj->entityTypeManager->getStorage('user');
    $user        = $userStorage->load($uid);
    $email       = $user->get('mail')->value;

    if (!strpos($email, '_noreply')) {

      $replaceEmail = str_replace('@', '_noreply@', $email);
      $password     = 'Sanity' . time();
      $user->set('pass', $password);
      $user->set('mail', $replaceEmail);

      if ($user->save()) {
        $context['results'][] = 1;
        $context['message']   = t("Changing email address on user id: @id",
        ["@id" => $uid]);
      }
    }

  }

  /**
   * Finished status of batch process.
   */
  public static function updateEmailsFinished($success, $results, $operations) {

    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'Email address were updated for one entity.',
        'Email address were updated for @count entities.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

}
