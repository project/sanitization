<?php

namespace Drupal\sanitization\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides settings for Sanitization module.
 */
class SanitizationSettingsForm extends ConfigFormBase {
  /**
   * The module manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs an AutologoutSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
  ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sanitization.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sanitization_settings';
  }

  /**
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config     = $this->config('sanitization.settings');
    $s3fsExists = $this->moduleHandler->moduleExists('s3fs');
    $smtpExists = $this->moduleHandler->moduleExists('smtp');

    $env = [
      'dev' => 'Development',
      'sit' => 'Testing (SIT)',
      'ppd' => 'Pre-Production (PPD)',
    ];

    $form['sanitization_env'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Sanitization Environments'),
      '#description'   => $this->t('Pick the environment to sanitize the data.'),
      '#options'       => $env,
      '#default_value' => $config->get('sanitization_env'),
    ];

    $form['sanitization_env_s3'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('S3 module exists'),
      '#value'         => $s3fsExists,
      '#default_value' => $config->get('sanitization_env_s3'),
      '#disabled'      => TRUE,
    ];

    $form['sanitization_env_smtp'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('SMTP module exists'),
      '#value'         => $smtpExists,
      '#default_value' => $config->get('sanitization_env_smtp'),
      '#disabled'      => TRUE,
    ];

    foreach ($env as $envKey => $envName) {

      $envSiteName     = $envKey . '_site_name';
      $envSiteMail     = $envKey . '_site_email';
      $envexcludeUids  = $envKey . '_excluded_uids';
      $tranlateEnvName = ['@envname' => $envName];

      $form[$envSiteName] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('@envname Site Name', $tranlateEnvName),
        '#description'   => $this->t('@envname Sanitize the site name.', $tranlateEnvName),
        '#default_value' => $config->get($envSiteName),
        '#states'        => [
          'visible' => [
            ':input[name="sanitization_env"]' => ['value' => $envKey],
          ],
        ],
      ];
      $form[$envSiteMail] = [
        '#type'          => 'email',
        '#title'         => $this->t('@envname Site Email address', $tranlateEnvName),
        '#description'   => $this->t('@envname Sanitize the site email address.', $tranlateEnvName),
        '#default_value' => $config->get($envSiteMail),
        '#states'        => [
          'visible' => [
            ':input[name="sanitization_env"]' => ['value' => $envKey],
          ],
        ],
      ];
      $form[$envexcludeUids] = [
        '#type'          => 'textarea',
        '#title'         => $this->t('@envname Exclude uids (comma seperated)', $tranlateEnvName),
        '#description'   => $this->t('@envname Exclude the uids to being sanitize except 0,1.', $tranlateEnvName),
        '#default_value' => $config->get($envexcludeUids),
        '#states'        => [
          'visible' => [
            ':input[name="sanitization_env"]' => ['value' => $envKey],
          ],
        ],
      ];

      /*
       * Form field for S3 configuration
       */
      if ($s3fsExists == 1) {

        $envs3FieldSet   = $envKey . '_s3';
        $envs3AccessKey  = $envKey . '_s3access_key';
        $envs3SecretKey  = $envKey . '_s3secret_key';
        $envs3bucketName = $envKey . '_bucket_name';

        $form[$envs3FieldSet] = [
          '#type'   => 'details',
          '#title'  => $this->t('@envname S3 Settigns', $tranlateEnvName),
          '#open'   => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envs3FieldSet][$envs3AccessKey] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('@envname S3 Access Key', $tranlateEnvName),
          '#description'   => $this->t('@envname S3 Access Key', $tranlateEnvName),
          '#default_value' => $config->get($envs3AccessKey),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envs3FieldSet][$envs3SecretKey] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('@envname S3 Secret Key', $tranlateEnvName),
          '#description'   => $this->t('@envname S3 Secret Key', $tranlateEnvName),
          '#default_value' => $config->get($envs3SecretKey),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envs3FieldSet][$envs3bucketName] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('@envname S3 bucket name', $tranlateEnvName),
          '#description'   => $this->t('@envname S3 bucket name', $tranlateEnvName),
          '#default_value' => $config->get($envs3bucketName),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
      }

      /*
       * Form field for SMTP configuration.
       */
      if ($smtpExists == 1) {

        $envSmtpFieldSet  = $envKey . '_smtp';
        $envSmtpHostName  = $envKey . '_smtp_hostname';
        $envSmtpUsername  = $envKey . '_smtp_username';
        $envsSmtpPassword = $envKey . '_smtp_password';
        $envsSmtpFrom     = $envKey . '_smtp_from';
        $envsSmtpFromname = $envKey . '_smtp_fromname';

        $form[$envSmtpFieldSet] = [
          '#type'   => 'details',
          '#title'  => $this->t('@envname SMTP Settigns', $tranlateEnvName),
          '#open'   => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envSmtpFieldSet][$envSmtpHostName] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('@envname SMTP Server', $tranlateEnvName),
          '#description'   => $this->t('@envname SMTP Server (Host Name)', $tranlateEnvName),
          '#default_value' => $config->get($envSmtpHostName),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envSmtpFieldSet][$envSmtpUsername] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('@envname SMTP Username', $tranlateEnvName),
          '#description'   => $this->t('@envname SMTP Username', $tranlateEnvName),
          '#default_value' => $config->get($envSmtpUsername),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envSmtpFieldSet][$envsSmtpPassword] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('@envname SMTP Password', $tranlateEnvName),
          '#description'   => $this->t('@envname SMTP Password', $tranlateEnvName),
          '#default_value' => $config->get($envsSmtpPassword),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envSmtpFieldSet][$envsSmtpFrom] = [
          '#type'          => 'email',
          '#title'         => $this->t('@envname SMTP E-mail from address', $tranlateEnvName),
          '#description'   => $this->t('@envname SMTP E-mail from address', $tranlateEnvName),
          '#default_value' => $config->get($envsSmtpFrom),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
        $form[$envSmtpFieldSet][$envsSmtpFromname] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('@envname SMTP E-mail from name', $tranlateEnvName),
          '#description'   => $this->t('@envname SMTP E-mail from name', $tranlateEnvName),
          '#default_value' => $config->get($envsSmtpFromname),
          '#states'        => [
            'visible' => [
              ':input[name="sanitization_env"]' => ['value' => $envKey],
            ],
          ],
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $env = $values['sanitization_env'];
    if ($values['sanitization_env_s3'] == 1) {
      $s3Validation    = [];
      $envs3FieldSet   = $env . '_smtp';
      $envs3AccessKey  = $env . '_s3access_key';
      $envs3SecretKey  = $env . '_s3secret_key';
      $envs3bucketName = $env . '_bucket_name';

      $s3Validation[] = $values[$envs3AccessKey];
      $s3Validation[] = $values[$envs3SecretKey];
      $s3Validation[] = $values[$envs3bucketName];

      $emptyCount = array_filter($s3Validation);

      if (count($emptyCount) > 0 && count($emptyCount) < 3) {
        $form_state->setErrorByName($envs3FieldSet, $this->t('You must enter all S3 require details or left blank.'));
        $form_state->setErrorByName($envs3AccessKey, '');
        $form_state->setErrorByName($envs3SecretKey, '');
        $form_state->setErrorByName($envs3bucketName, '');
      }
    }

    if ($values['sanitization_env_smtp'] == 1) {
      $smtpValidation   = [];
      $envSmtpFieldSet  = $env . '_smtp';
      $envSmtpHostName  = $env . '_smtp_hostname';
      $envSmtpUsername  = $env . '_smtp_username';
      $envsSmtpPassword = $env . '_smtp_password';
      $envsSmtpFrom     = $env . '_smtp_from';
      $envsSmtpFromname = $env . '_smtp_fromname';

      $smtpValidation[] = $values[$envSmtpHostName];
      $smtpValidation[] = $values[$envSmtpUsername];
      $smtpValidation[] = $values[$envsSmtpPassword];
      $smtpValidation[] = $values[$envsSmtpFrom];
      $smtpValidation[] = $values[$envsSmtpFromname];

      $emptyCount = array_filter($smtpValidation);

      if (count($emptyCount) > 0 && count($emptyCount) < 5) {
        $form_state->setErrorByName($envSmtpFieldSet, $this->t('You must enter all SMTP require details or left blank.'));
        $form_state->setErrorByName($envSmtpHostName, '');
        $form_state->setErrorByName($envSmtpUsername, '');
        $form_state->setErrorByName($envSmtpUsername, '');
        $form_state->setErrorByName($envsSmtpFrom, '');
        $form_state->setErrorByName($envsSmtpFromname, '');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values         = $form_state->getValues();
    $envSiteName    = $values['sanitization_env'] . '_site_name';
    $envSiteMail    = $values['sanitization_env'] . '_site_email';
    $envexcludeUids = $values['sanitization_env'] . '_excluded_uids';

    $sanitizationSettigns = $this->config('sanitization.settings');
    $sanitizationSettigns->set('sanitization_env', $values['sanitization_env']);
    $sanitizationSettigns->set($envSiteName, $values[$envSiteName]);
    $sanitizationSettigns->set($envSiteMail, $values[$envSiteMail]);
    $sanitizationSettigns->set($envexcludeUids, $values[$envexcludeUids]);

    if ($values['sanitization_env_s3'] == 1) {
      $envs3AccessKey = $values['sanitization_env'] . '_s3access_key';
      $envs3SecretKey = $values['sanitization_env'] . '_s3secret_key';
      $envs3bucketName = $values['sanitization_env'] . '_bucket_name';

      $sanitizationSettigns->set($envs3AccessKey, $values[$envs3AccessKey]);
      $sanitizationSettigns->set($envs3SecretKey, $values[$envs3SecretKey]);
      $sanitizationSettigns->set($envs3bucketName, $values[$envs3bucketName]);
    }

    if ($values['sanitization_env_smtp'] == 1) {
      $envSmtpHostName  = $values['sanitization_env'] . '_smtp_hostname';
      $envSmtpUsername  = $values['sanitization_env'] . '_smtp_username';
      $envsSmtpPassword = $values['sanitization_env'] . '_smtp_password';
      $envsSmtpFrom     = $values['sanitization_env'] . '_smtp_from';
      $envsSmtpFromname = $values['sanitization_env'] . '_smtp_fromname';

      $sanitizationSettigns->set($envSmtpHostName, $values[$envSmtpHostName]);
      $sanitizationSettigns->set($envSmtpUsername, $values[$envSmtpUsername]);
      $sanitizationSettigns->set($envsSmtpPassword, $values[$envsSmtpPassword]);
      $sanitizationSettigns->set($envsSmtpFrom, $values[$envsSmtpFrom]);
      $sanitizationSettigns->set($envsSmtpFromname, $values[$envsSmtpFromname]);
    }

    $sanitizationSettigns->save();

    // Update the site information.
    $systemSite = $this->configFactory->getEditable('system.site');

    if (!empty($values[$envSiteName])) {
      $systemSite->set('name', $values[$envSiteName]);
    }

    if (!empty($values[$envSiteMail])) {
      $systemSite->set('mail', $values[$envSiteMail]);
    }

    $systemSite->save(TRUE);

    // Update the S3 configuration.
    if ($values['sanitization_env_s3'] == 1) {
      $s3Config = $this->configFactory->getEditable('s3fs.settings');
      $envs3AccessKey = $values['sanitization_env'] . '_s3access_key';
      $envs3SecretKey = $values['sanitization_env'] . '_s3secret_key';
      $envs3bucketName = $values['sanitization_env'] . '_bucket_name';

      if ($envs3AccessKey != '' && $envs3SecretKey != '' && $envs3bucketName != '') {
        $s3Config->set('access_key', $values[$envs3AccessKey]);
        $s3Config->set('secret_key', $values[$envs3SecretKey]);
        $s3Config->set('bucket', $values[$envs3bucketName]);
        $s3Config->save(TRUE);
      }
    }

    // Update the SMTP configuration.
    if ($values['sanitization_env_smtp'] == 1) {
      $smtpConfig       = $this->configFactory->getEditable('smtp.settings');
      $envSmtpHostName  = $values['sanitization_env'] . '_smtp_hostname';
      $envSmtpUsername  = $values['sanitization_env'] . '_smtp_username';
      $envsSmtpPassword = $values['sanitization_env'] . '_smtp_password';
      $envsSmtpFrom     = $values['sanitization_env'] . '_smtp_from';
      $envsSmtpFromname = $values['sanitization_env'] . '_smtp_fromname';

      if ($envSmtpHostName != '' &&
       $envSmtpUsername != '' &&
       $envsSmtpPassword != '' &&
       $envsSmtpFrom != '' &&
       $envsSmtpFromname != ''
       ) {
        $smtpConfig->set('smtp_host', $values[$envSmtpHostName]);
        $smtpConfig->set('smtp_username', $values[$envSmtpUsername]);
        $smtpConfig->set('smtp_password', $values[$envsSmtpPassword]);
        $smtpConfig->set('smtp_from', $values[$envsSmtpFrom]);
        $smtpConfig->set('smtp_fromname', $values[$envsSmtpFromname]);
        $smtpConfig->save(TRUE);
      }
    }
  }

}
